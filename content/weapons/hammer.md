---
title: "Hammer"
weight: 10
---

The Hammer is a melee, semi-automatic weapon.  
It is one of the 2 always accessible weapons, and it is probably the one you will be using the most.  
Its main purpose is to unfreeze other tees and manipulate their movement.

| Quickinfo  |       |
| ---------- | ----- |
| Gameskin   | ![Hammer](/images/hammer.png) |
| Crosshair  | ![Hammer crosshair](/images/hammer-crosshair.png) |
| Fire delay | 120ms (320ms when hitting another tee) |

### General Information
As the only melee weapon, the hammer has a relatively short range.  
Any tees hit by the its generous hitbox will

- get unfrozen
- get pushed away from the hammering tee

The range of the hammer allows you to just barely hit tees behind a 1 tile thick wall.  

<table>
	<tr>
		<th></th>
		<th>Left</th>
		<th>Not moving</th>
		<th>Right</th>
	</tr>
	<tr>
		<td>Without jump</td>
		<td>{{% vid hammer-left-nojump %}}</td>
		<td>{{% vid hammer-nomovement-nojump %}}</td>
		<td>{{% vid hammer-right-nojump %}}</td>
	</tr>
	<tr>
		<td>With jump</td>
		<td>{{% vid hammer-left-jump %}}</td>
		<td>{{% vid hammer-nomovement-jump %}}</td>
		<td>{{% vid hammer-right-jump %}}</td>
	</tr>
</table>


| Result            | `Left` | `Not moving` | `Right` | Jump height |
| ----------------- | ------ | ------------ | ------- | ----------- |
| `Without jumping` | 2#11   | 1#22         | 4#12    | 2#00        |
| `Jumping`         | 2#23   | 1#28         | 5#03    | 5#22        |

### Unfreezing
When you hit another tee with the, it will immediately unfreeze the tee.  

#### 1-Tick-Unfreeze
If the tee you are hammering is currently in a freeze tile, it will get directly frozen again.
However, just like when hit by the Laser Rifle, the hit tee will unfrozen for a brief moment in which it can:

- jump
- move (only very little)
- fire any weapon

With all of the other weapons, the briefly unfrozen tee can hold down the fire button to fire directly on getting unfrozen by the hammer.  
Using this you can create chain reactions using the 1-tick-unfreeze.

### Movement Using Hammering

On top of being unfrozen, the hit tee will get good amount of speed.
While there are a lot of tricks with the hammer alone, combining it with other utilities allows for many more tricks.

#### Hammerrun

Hammerrun combines running speed with the horizontal speed from the hammer.

- the tee in the back initiates it by walking against the tee in the front
- the one in the front can then also begin walking in the same direction
- now the tee in the back can hammer the one in front to give it speed

{{% vid hammerrun %}}

#### Hammerjump

Hammerjump combines jumping with the vertical speed from the hammer.
It is essential that the hammer hit happens after that jump.

{{% vid hammerjump %}}

### Advanced Behaviour
- while the Hammer is a semi-automatic weapon, it can be held down to shoot directly on unfreeze
- counterintuitively, the hammer will never cause downwards momentum, even when the hammering tee is on top of the other tee. It will only cause sideways and upwards momentum
