---
title: "Shotgun"
weight: 30
---

The Shotgun fires a straight beam and is a mobility focused, automatic weapon.
The beam will reflect off walls and ends by either hitting a tee or by reaching its maximum range.

If a tee is hit (also yourself), it will be pulled towards the beam.
The pull can be used to build up speed in any direction.

[Video of indirect shotgun hit on other tee]

| Quickinfo  |       |
| ---------- | ----- |
| Gameskin   | ![Shotgun](/images/shotgun.png) |
| Crosshair  | ![Shotgun crosshair](/images/shotgun-crosshair.png) |
| Fire delay | 500ms |

### Horizontal Movement

To build up speed horizontally, a tee needs to be hit by a shotgun shot coming its way.

#### With 2 Tees

- get the 2 tees to face each other at a distance
- first tee starts running
- second tee shotguns the first tee

Keep in mind that its much more efficient if the first tee reaches full walking speed before it gets shot!

{{% vid shotgun-2 %}}

#### By Yourself

- seek out a block that you can hit with a horizontal shotgun shot
- run towards it, lign up your shot
- hit yourself by shooting the block and having the shotgun beam reflect off it
- dodge the block (usually by jumping)

Just like with 2 tees, its much more efficient if you make sure you've reached full walking speed before your shoot yourself indirectly.

{{% vid shotgun-1 %}}

### Vertical Movement

Note that if we have 2 things that lift up upwards (eg. a jump, shotgun shot from above, hook, etc.), using them in combination will have a much greater effect.

#### With 2 Tees

When you need to pull up a **unfrozen** tee that is out of hooking range: 
- lower tee jumps 
- upper tee shoots the lower tee right after

When you need to pull up a **frozen** tee that is out of hooking range:

- shoot the lower tee a couple of times
- hook it, once it is in reach

{{% vid shotgun-4 %}}

#### By Yourself

Similar to gaining horizontal movement by yourself using the shotgun:

- seek out a block that you can hit with an vertical shotgun shot
- aim at the block, just slightly to the side
- jump, shoot right after
- dodge the block by moving to the side

{{% vid shotgun-up %}} 

Without decend starting momentum (here the jump), the shotgun shot won't get you very high.
However when you already have some upwards momentum from another source (eg. being thrown up by another tee), you can go through the same steps leaving out the jump.

### Advanced Indirect Shots

In harder maps you will be required to build up horizontal or vertical movement without a convenient placed block to reflect your shotgun shot off.

#### Horizontally

To build up horizontal speed, you can reflect your own beam off the floor you are walking on.
With the correct, very precise angle, the beam will push you forwards, giving you the desired boost.

- aim forward and a bit downwards
- get to walking speed
- shoot

It didn't work?

- If you were slowed down by the beam or if the beam reflected off behind you, aim higher.
- If the beam missed you and went in front of your tee, aim lower.

**Keep in mind that the correct angles changes with your speed!**

{{% vid shotgun-6 %}}

#### Vertically

Similar to the horizontal trick, you can also use a wall to get vertical momentum with the shotgun.

While your horizontal speed usually stays constant while you are running, your vertical speed changes steadily due to gravity.
So while the trick is the exact same as the horizontal one, the correct angle for your shot changes constantly during your jump.

- stand directly next to the wall
- aim up and a bit to the wall
- jump, shoot right after

It didn't work?

- If you were dragged down by the beam or if the beam went above you, aim further into the wall.
- If the beam missed you and went in below your tee, aim further up.

{{% vid shotgun-7 %}} 
{{% vid shotgun-up-wall %}}

### Advanced Behaviour

- whenever a shotgun beam hits a will, it will delay briefly before reflecting off it
- since the Shotgun is an automatic weapon it can be held down to shoot indefinitely, also directly on unfreeze
- the beam does not in fact drag you in the direction it came from. Instead, it pulls your tee towards the origin of it, so either where it was shot from or from the last point it reflected off. This is also the reason why the advanced indirect shots work.
