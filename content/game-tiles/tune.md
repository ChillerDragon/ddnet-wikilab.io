---
title: "Tune Layer"
explain: true
weight: 70
---

#### Tile representations of the 'tune' layer in the editor.

Contains the tune zone tile.
Necessary due to its unique properties. 

{{% explain file="static/explain/tune.svg" %}}
