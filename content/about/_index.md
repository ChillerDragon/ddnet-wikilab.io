---
title: "About"
weight: 1000
---

## Contributing

On each page you can click on 
<a class="github-link" title='Edit this page' href="#" target="blank" onclick="return false">
    <i class="fas fa-code-branch"></i>
    <span>Edit this page</span>
</a>
in the top right corner to open the corresponding file in Gitlab.

You can then click on "Edit" to open the [Gitlab Web Editor][1] to create merge request.

[1]: https://docs.gitlab.com/ee/user/project/repository/web_editor.html

### See also:

- [Editing pages](editing)
- [Creating videos](videos)
- [Modifying game tile explanations](game-tiles)
