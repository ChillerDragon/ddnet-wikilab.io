---
title: "Movement"
---

Gravitation: Every tick the acceleration of 0.5 is added to the y velocity (downwards).

## Input (horizontal movement):

* Pressing nothing:
  * Air: Every tick `velocy.x = velocy.x * 0.95`
  * Ground: Every tick `velocy.x = velocy.x * 0.5`
* Pressing key in opposite direction:
  * Ground: Every Tick horizontal velocity decreases by 2 until 10 in the opposite direction is reached
  * Air: Every Tick horizontal velocity decreases by 1.5 until -5 in the opposite direction is reached
* Horizontal speed up by pressing `a` or `d`:
  * Ground: Every Tick horizontal velocity increases by 2 until 10 is reached
  * Air: Every Tick horizontal velocity increases by 1.5 until -5 is reached

## Max speed

Max speed: 6000 Sub-tiles/Tick (+speedup in one tick, e.g. gravity, jetpack, explosion).

##### `src/game/gamecore.cpp` (before calculating new position from velocity):

```c
if(length(m_Vel) > 6000)
	m_Vel = normalize(m_Vel) * 6000;
```

This is the bottle neck in vertical movement.
In horizontal movement a "ramp"-Value is multiplied to the horizontal speed.
Causing the tee to slow down and stop when getting too fast.
This is most commonly observed in speed ups.

The speed is rounded down to a precision of 1/256 every tick.
This is causing the tee to stop moving horizontal when having too much speed.

Todo:
* convert sub-tiles/tick into tiles/sec
* find max vel und real max vel (from where would a constant function fit)
* find speed, where the tee stops moving
* write about ramp speed, and add diagram

