#!/bin/bash

# example usage: ./multiple-exposure.sh input_vid.mp4 6
# where 6 is the frames per second to extract

rm -rf orig diff
mkdir orig
mkdir diff
ffmpeg -i $1 -vf fps=$2 orig/%d.png

cd orig
for img in *.png
do
    echo Calculate diff $img
    convert $img 1.png -fuzz 10% -compose Change-mask -composite ../diff/$img
done

cd ..
cp orig/1.png comp.png
cd diff
for img in $(ls *.png | sort -h | tac)
do
    echo $img
    composite -compose Dst_Over ../comp.png $img ../comp.png
done
