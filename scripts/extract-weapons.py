#!/usr/bin/env python3
import subprocess
import os

ASSETS = {
	"hammer": "128x96+64+32",
	"pistol": "128x64+64+128",
	"shotgun": "224x64+64+192",
	"grenade": "224x64+64+256",
	"ninja": "224x64+96+320",
	"laser": "224x96+64+384",
	"hammer-crosshair": "64x64+0+0",
	"pistol-crosshair": "64x64+0+128",
	"shotgun-crosshair": "64x64+0+192",
	"grenade-crosshair": "64x64+0+256",
	"ninja-crosshair": "64x64+0+320",
	"laser-crosshair": "64x64+0+384",
}

print(ASSETS)

for asset in ASSETS:
	print(asset)
	print(ASSETS[asset])
	subprocess.call(["magick", "game.png", "-crop", ASSETS[asset], asset+".png"])
